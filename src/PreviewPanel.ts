import * as vscode from 'vscode';
import { Document } from '@minidoc/core';

export default class PreviewPanel {
	private static html: string;

	private panel: vscode.WebviewPanel;

	constructor(
		private context: vscode.ExtensionContext,
		private document: Document
	) {
		this.create();
	}

	private create() {
		this.panel = vscode.window.createWebviewPanel(
			'minidoc-language-support',
			'Preview',
			vscode.ViewColumn.Two,
			{ enableScripts: true, localResourceRoots: [vscode.Uri.joinPath(this.context.extensionUri, 'media')] }
		);

		this.getHtml().then(result => this.panel.webview.html = result);

		this.registerListeners();
	}

	private getHtml(): Promise<string> {
		return new Promise<string>(resolve => {
			if (PreviewPanel.html) {
				resolve(PreviewPanel.html);
			}

			const htmlPath = vscode.Uri.joinPath(this.context.extensionUri, 'media', 'index.html');

			vscode.workspace.fs.readFile(htmlPath).then(buffer => {
				if (this.panel) {
					let html = buffer.toString();
					const rewriteUri = (value: string) => {
						const originalUri = value.substring(1, value.length - 1);
						const newUri = vscode.Uri.joinPath(this.context.extensionUri, 'media', originalUri);
						return this.panel.webview.asWebviewUri(newUri);
					};

					html = html.replace(/\"\/static\/[a-zA-Z0-9/\-\.]+\"/g, v => `"${rewriteUri(v)}"`);

					PreviewPanel.html = html;
					resolve(html);
				}
			});
		});
	}

	public setDocument(document: Document) {
		this.document = document;

		if (this.panel) {
			this.panel.webview.postMessage({ document: document });
		}
	}

	private registerListeners() {
		this.panel.webview.onDidReceiveMessage(
			message => {
				switch (message.command) {
					case 'ready':
						this.setDocument(this.document);
						break;
				}
			}
		);
	}
}
