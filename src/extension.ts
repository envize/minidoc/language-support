import * as vscode from 'vscode';
import Processor from './Processor';
import PreviewPanel from './PreviewPanel';

const processor = new Processor();
let panel: PreviewPanel;

export function activate(context: vscode.ExtensionContext) {

	const openPreviewPane = () => {
		const textDocument = vscode.window.activeTextEditor?.document;

		if (processor.isSupported(textDocument)) {
			const document = processor.process(textDocument.getText());
			panel = new PreviewPanel(context, document);
		} else {
			vscode.window.showInformationMessage('There is no MiniDoc file in focus.');
		}
	};

	vscode.workspace.onDidSaveTextDocument(textDocument => {
		if (panel && processor.isSupported(textDocument)) {
			const document = processor.process(textDocument.getText());
			panel.setDocument(document);
		}
	});

	context.subscriptions.push(vscode.commands.registerCommand(
		'minidoc-language-support.showPreviewPane',
		openPreviewPane
	));
}

export function deactivate() { }
