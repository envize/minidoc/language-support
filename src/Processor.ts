import * as vscode from 'vscode';
import { process } from '@minidoc/processor';
import { Document } from '@minidoc/core';

const minidocLanguageId = 'minidoc';

export default class Processor {

	public isSupported(document: vscode.TextDocument): boolean {
		return document?.languageId === minidocLanguageId || false;
	}

	public process(input: string): Document {
		return process(input);
	}
}
